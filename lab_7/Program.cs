﻿using System;
using System.IO;
using System.Collections.Generic;

namespace lab_7
{
    class Program
    {

        class Route
        {
            private int[] points;
            private int Distance;
            private int StartPoint;

            public Route(int number) { points = new int[number]; }
            public void SetRoute(int[] array_points)
            { 
                array_points.CopyTo(points, 0);
                StartPoint = points[0];
            
            }
            public int GetPoint(int index) { return points[index]; }
            public int[] GetPoints() { return points; }
            public void SetDistance(Map map)
            {
                Distance = map.GetDistance(points);
            }

            public int GetDistance() { return Distance; }

            public void Shuffle(Route orig, int numbers)//кол-во изменений
            {
                Random rnd = new Random();

                SetRoute(orig.points);

                int[] index_edits = new int[numbers * 2];//кол-во индексов под изменения
                for (int i = 0; i < index_edits.Length; i++)
                    index_edits[i] = -1;

                int temp;

                for (int i = 0; i < index_edits.Length; i ++)
                {
                    do
                    {
                        temp = rnd.Next(1, orig.points.Length );
                    } while (Array.FindIndex(index_edits, x => x == temp) != -1);


                    index_edits[i] = temp;
                }
                    


                for (int i = 0; i < index_edits.Length - 1; i++)
                {
                    temp = points[index_edits[i]];
                    points[index_edits[i]] = points[index_edits[i + 1]];
                    points[index_edits[i + 1]] = temp;

                }
            }

            public void CrossingOver(Route parents1, Route parents2,int BreakPoint, int ParentFirst) //скрещивание
            {
                // for(int i = 0; )
                if (ParentFirst == 0)
                {
                    Array.Copy(parents1.points, points, BreakPoint);

                    int k = BreakPoint;

                    for (int i = BreakPoint; i < points.Length; i++)
                        if (Array.Exists(points, x => x == parents2.points[i]))
                        {
                            if (Array.Exists(points, x => x == parents1.points[k]))
                                do { k++; } while (Array.Exists(points, x => x == parents1.points[k]) && k < points.Length);
                            points[i] = parents1.points[k];
                            k++;
                        }
                        else
                            points[i] = parents2.points[i];
                }
                else
                {
                    Array.Copy(parents2.points, points, BreakPoint);

                    int k = BreakPoint;

                    for (int i = BreakPoint; i < points.Length; i++)
                        if (Array.Exists(points, x => x == parents1.points[i]))
                        {
                            if (Array.Exists(points, x => x == parents2.points[k]))
                                do { k++; } while (Array.Exists(points, x => x == parents2.points[k]) && k < points.Length);
                            points[i] = parents2.points[k];
                            k++;
                        }
                        else
                            points[i] = parents1.points[i];

                }

                bool f = Check();

            }

            public void Mutation(int procent)
            {
                Random rnd = new Random();
                int temp = rnd.Next(1, 100);

                if (temp < procent)
                {
                    Route temp_r = new Route(points.Length);
                    temp_r.SetRoute(points);

                    Shuffle(temp_r, 1);

                    SetRoute(temp_r.points);
                }
            }

            private bool Check()
            {

                for (int i = 0; i < points.Length; i++)
                    if (Array.FindAll(points, x => x == i).Length > 1) return true;
                return false;
            }
        }

        class Map 
        {
            private int NumberPoints;
            private int[,] Ways;
            private const int MinLen = 1;
            private const int MaxLen = 10;

            public Map(int NumberPoints)
            {
                this.NumberPoints = NumberPoints;
                Ways = new int[NumberPoints, NumberPoints];

            }

            public int GetDistance(int[] points)
            {
                int res = 0;
                for (int i = 0; i < points.Length - 1; i++) 
                {
                    res += Ways[points[i] - 1, points[i + 1] - 1];
                }

                res += Ways[points[0] - 1, points[points.Length - 1] - 1 ];

                return res;
            }

            public void RandomMap()
            {
                Random rnd = new Random();

                for (int i = 0; i < NumberPoints; i++)
                    for (int j = 0; j < NumberPoints; j++)
                        if (i == j) continue;
                        else
                        {
                            Ways[i, j] = Ways[j, i] = rnd.Next(MinLen, MaxLen);
                        
                        }

            }

            public void show()
            {
                for (int i = 0; i < NumberPoints; i++)
                {

                    for (int j = 0; j < NumberPoints; j++) 
                        Console.Write(Ways[i,j] + "\t");

                    Console.WriteLine();
                }


            }

        }

        class GeneticAlgorithm
        {
            private int mutation;          //процент мутации
            private int NumberIndividuals; //количество особей в популяции

            private int NumberParents;     //количество родителей при скрещивании
            private int NumbersCities;

            private Map GeneralMap;
            private List<Route> population;

            public GeneticAlgorithm(int CitiesCols, int ParentsCols, int Mutation)
            {
                NumbersCities = CitiesCols;
                NumberParents = ParentsCols;
                mutation = Mutation;

                population = new List<Route>(NumbersCities);

                Route temp;
                for (int i = 0; i < NumbersCities; i++)
                {
                    temp = new Route(NumbersCities);
                    population.Add(temp);
                }

                GeneralMap = new Map(NumbersCities);
                GeneralMap.RandomMap();

                //int[] points = {1,2,3,4,5,6,7,8,9,10 };
                //population[0].SetRoute(points);
                //points.CopyTo(population[0].route, 0);
                //;

            }

            private void StartGA()
            {
                const int StartEdits = 1; //количество изменений в начальной популяции

                int[] start_points = new int[NumbersCities];
                for (int i = 0; i < NumbersCities; i++)
                    start_points[i] = i + 1;

                population[0].SetRoute(start_points);

                Route temp = new Route(NumbersCities);
                bool flag = false;

                for (int i = 1; i < NumbersCities; i++)
                {
                    do
                    {
                        temp.Shuffle(population[0], StartEdits);
                        foreach (Route r in population)
                            if (r.GetPoints() == temp.GetPoints())
                                flag = true;
                            else
                                flag = false;


                    } while (flag == true);

                    population[i].SetRoute(temp.GetPoints());
                }




            }

            private void PopulationUp()
            {
                List<Route> childrens = new List<Route>(NumberParents);
                int[] parents = RandomParents();
                int break_point;
                int parent_first = 0;
                Random rnd = new Random();

                Route temp;
                for (int i = 0; i < NumberParents / 2; i += 2)
                {
                    break_point = rnd.Next(0, NumbersCities);
                    for (int j = 0; j < 2; j++)
                    {
                        temp = new Route(NumbersCities);

                        temp.CrossingOver(population[parents[i]], population[parents[i + 1]], break_point, j);
                        //temp.Mutation(mutation);

                        childrens.Add(temp);
                    }

                }

                population.AddRange(childrens);

            }

            public void PopulationDown()
            {
                for (int i = 0; i < population.Count; i++)
                    population[i].SetDistance(GeneralMap);

                population.Sort((x, y) => x.GetDistance().CompareTo(y.GetDistance()));
                population.RemoveRange(NumbersCities, population.Count - NumbersCities);

            }



            private int[] RandomParents()
            {
                int[] parents = new int[NumberParents];
                int temp;
                Random rnd = new Random();

                for (int i = 0; i < NumberParents; i++)
                {
                    do
                    {
                        temp = rnd.Next(1, NumbersCities);
                    } while (Array.Exists(parents, x => x == temp));

                    parents[i] = temp;
                }

                return parents;
            }

            private int[] AdaptedParents()
            {
                int[] parents = new int[NumberParents];
                for (int i = 0; i < population.Count; i++)
                    population[i].SetDistance(GeneralMap);

                population.Sort((x, y) => x.GetDistance().CompareTo(y.GetDistance()));

                for (int i = 0; i < NumberParents; i++)
                {
                    parents[i] = i;
                }

                return parents;
            }
            

            public void Evolutions(int epochs)
            {
                int[] MinDistances = new int[epochs];
                int[] AvgDistances = new int[epochs];
                int[] MaxDistances = new int[epochs];

                StartGA();
                for (int i = 0; i < epochs; i++)
                {
                    PopulationUp();
                    PopulationDown();

                    MinDistances[i] = population[0].GetDistance();
                    AvgDistances[i] = population[(population.Count - 1) / 2].GetDistance();
                    MaxDistances[i] = population[population.Count - 1].GetDistance();

                    Console.WriteLine("Эпоха: " + i);
                    for (int j = 0; j < population.Count; j++)
                    {
                        Show(j);
                        Console.WriteLine();
                    }
                    Console.WriteLine();
                }

                CreateReport(MinDistances, AvgDistances, MaxDistances, epochs);
            }

            public void CreateReport(int[] mins, int[] avgs, int[] maxs, int epochs)
            {
                StreamWriter fstream = new StreamWriter("отчет.txt", false);
                fstream.WriteLine("Эпоха\tМин.расстояние\tСр.расстояние\tМакс.расстояние");

                for (int i = 0; i < epochs; i++)
                {
                    fstream.WriteLine(i + "\t" + mins[i] + "\t" + avgs[i] + "\t" + maxs[i]);
                }
                fstream.Close();

            }

            public void Show(int index)
            {

                Console.Write("[" + index + "]: ");
                foreach (int i in population[index].GetPoints())
                    Console.Write(i + " ");
                Console.Write("\t" + population[index].GetDistance());

            }
        }

        static void Main(string[] args)
        {

            GeneticAlgorithm ts = new GeneticAlgorithm(40, 3, 40);
            ts.Evolutions(20);

        }
    }
}
